package com.ezb.alp.service;

import static org.junit.Assert.assertEquals
import static org.junit.Assert.assertNotNull
import static org.junit.Assert.assertNotSame
import static org.junit.Assert.assertNull

import org.mockito.Mock

import spock.lang.Specification

import com.ezb.alp.Identity
import com.ezb.alp.User
import com.ezb.alp.repository.CustomerRepository


/**
 * TDD test using Spock Framework
 * Class used to test CustomerService methods
 * @author Jerry Rydere
 *
 */
class CustomerServiceTest extends Specification{

	/**
	 * Customer Service Declaration
	 */
	private CustomerService customerService;
	
	/**
	 * CustomerRepository Service Declaration
	 */
	private CustomerRepository customerRepository;

	/**
	 * Identity Entity Declaration
	 */
	private Identity identity

	/**
	 * User Entity Declaration
	 */
	private User user;

	/**
	 * Setting the Identity entity values across the class
	 * @return
	 */
	def setup() {
		identity = new Identity();
		user = new User();
		identity.setUserName("Jerry");
		identity.setPassword("jerry123");
		identity.setPlatinum(true);
		customerRepository = Mock();
		customerService = new CustomerService(customerRepository:customerRepository);
		
	}

	def 'login with valid Username and Password'() {
		given:"User Need To Login To The Application"
			identity.setUserName("Jerry");
			identity.setPassword("jerry123");
			identity.setPlatinum(true);
		when:"Passing Valid UserName and Password as Input"
			customerService.login(identity);
		then:"User Is Authenticatied And Logs In"
			customerRepository.findByUserNameAndPassword(identity.getUserName(),identity.getPassword()) >>  identity;
	}
	
	def 'login with invalid Username'() {
		given:"User Need To Login To The Application"
			identity.setUserName("xxxyyy");
			identity.setPassword("jerry123");
			identity.setPlatinum(true);
		when:"Passing In-Valid UserName and Valid Password as Input"
			customerService.login(identity);
		then:"User Is Not Authenticatied Due To Incorect Username"
			customerRepository.findByUserNameAndPassword(identity.getUserName(),identity.getPassword()) >>  identity;
			assertNotSame("invalid", identity.getUserName());
	}
	
	
	def 'login with invalid Password'() {
		given:"User Need To Login To The Application"
			identity.setUserName("Jerry");
			identity.setPassword("xxxzzz");
			identity.setPlatinum(true);
		when:"Passing In-Valid Password and Valid UserName as Input"
			customerService.login(identity);
		then:"User Is Not Authenticatied And CouldNot Logs In To The Application"
			customerRepository.findByUserNameAndPassword(identity.getUserName(),identity.getPassword()) >>  identity;
			assertNotSame("invalid", identity.getPassword());
	}
	
	def 'login with invalid Username and Password'() {
		given:"User Need To Login To The Application"
			identity.setUserName("yyyzzz");
			identity.setPassword("xxxzzz");
			identity.setPlatinum(true);
		when:"Passing In-Valid Password and Valid UserName as Input"
			customerService.login(identity);
		then:"User Is Not Authenticatied And CouldNot Logs In To The Application"
			customerRepository.findByUserNameAndPassword(identity.getUserName(),identity.getPassword()) >>  identity;
			assertNotSame("invalid", identity.getUserName());
			assertNotSame("invalid", identity.getPassword());
	}

	def 'check the user is valid platinum customer'() {
		given:"Need To Check The Login User is Valid Platinum Customer"
			identity = Mock();
		when:"User Is a Platinum Customer"
			identity.isPlatinum();
		then:"Confirms The User Is Valid Platinum Customer"
			identity.isPlatinum() >>  identity;
			//identity.isPlatinum() >> new Identity(true:identity.isPlatinum());
	}
	
	def 'check the user as Invalid platinum customer'() {
		given:"Need To Check The Login User is InValid Platinum Customer"
			identity = Mock();
			identity.setPlatinum(false);
		when:"User Is a Invalid Platinum Customer"
			identity.isPlatinum();
		then:"Confirms The User Is InValid Platinum Customer"
			identity.isPlatinum() >>  identity;
			//assertNotSame("false", identity.isPlatinum());
			assertEquals(false, identity.isPlatinum());
			//identity.isPlatinum() >> new Identity(true:identity.isPlatinum());
	}
}
