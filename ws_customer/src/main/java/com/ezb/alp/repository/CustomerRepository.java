package com.ezb.alp.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ezb.alp.Identity;

@Repository
public interface CustomerRepository extends MongoRepository<Identity, String> {
	Identity findByUserNameAndPassword(@Param("userName") String userName, @Param("password") String password);
}

