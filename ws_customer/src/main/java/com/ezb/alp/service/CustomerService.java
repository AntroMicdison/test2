package com.ezb.alp.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.ezb.alp.Identity;
import com.ezb.alp.User;
import com.ezb.alp.repository.CustomerRepository;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;


@RestController
@RequestMapping("/")
public class CustomerService implements ICustomerService {
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private CustomerRepository customerRepository;

	@ApiOperation(
		    value = "Login Verification Service",
		    notes =
		        "Service for User Authentication and Login into the Application",
		    response = User.class)
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public @ResponseBody User login(@ApiParam(value = "User Credentials", required = true) @RequestBody Identity identity) {
		Identity customer = customerRepository.findByUserNameAndPassword(identity.getUserName(),
				identity.getPassword());
		if (customer.isPlatinum())
		{
			return customer.getUser();
		}
		else
		{
			return null;
		}
	}

}

