package com.ezb.alp.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.ezb.alp.User;

@Repository
public interface UserRepository extends MongoRepository<User, String> {
}