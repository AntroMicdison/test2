package com.ezb.alp.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.ezb.alp.Application;
import com.ezb.alp.model.LoanApplication;
import com.ezb.alp.service.LoanApplicationService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/")
public class LoanApplicationController {

	private Logger logger = LoggerFactory.getLogger(LoanApplicationController.class);

	@Autowired
	LoanApplicationService loanApplicationService;

	/**
	 * Save Application Details
	 * 
	 * @param application
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(
		    value = "Submit Loan Application",
		    notes =
		        "Service for Create and Submit a Loan Applicaiton Details",
		    response = Application.class)
	@RequestMapping(value = "/loanapplication", method = RequestMethod.POST)
	public @ResponseBody Application saveLoanApplicationDetails(@ApiParam(value = "Loan Application Details", required = true) @RequestBody LoanApplication loanApplication) throws Exception {
		Application app = null;
		app = loanApplicationService.saveLoanApplicationDetails(loanApplication);
		return app;
	}

}
