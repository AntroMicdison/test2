package com.ezb.alp.controller

import spock.lang.Specification

import com.ezb.alp.Application
import com.ezb.alp.Branch
import com.ezb.alp.User
import com.ezb.alp.model.LoanApplication
import com.ezb.alp.repository.ApplicationRepository
import com.ezb.alp.service.LoanApplicationService

class LoanApplicationControllerTest  extends Specification {
	
	
	/**
	 * LoanApplicationController Declaration
	 */
	private LoanApplicationController loanApplicationController;
	
	/**
	 * LoanApplicationController Declaration
	 */
	private LoanApplicationController loanApplVehicleController;
	
	/**
	 * LoanApplicationService Declaration
	 */
	private LoanApplicationService loanApplicationService;
	
	/**
	 * ApplicationRepository Declaration
	 */
	private ApplicationRepository applicationRepository;
	
	/**
	 * Application Entity Declaration
	 */
	private Application application;
	
	/**
	 * User Entity Declaration
	 */
	private User user;
	
	/**
	 * LoanApplication VO Declaration
	 */
	private LoanApplication loanApplication;
	
	
	/**
	 * Setting the User entity values across the class
	 * @return
	 */
	def setup() {
		application = new Application();
		application.setApplicationId("20160800000001");
		application.setRequestedAmt(250000);
		application.setLoanTerm(3);
		loanApplication = new LoanApplication();
		user = new User();
		user.setUserName("Jerry");
		loanApplication.setUser(user);
	}
	
	def 'save loanApplicationDetail by given loanApplication value object'() {
		given:"Save The Loan Application Data"
			loanApplicationService = Mock();
			loanApplicationController = new LoanApplicationController(loanApplicationService:loanApplicationService);
			LoanApplication loanAppl = Mock();
		when:"Set The LoanApplication Value Object and Pass It As Parameter"
			loanApplicationController.saveLoanApplicationDetails(loanAppl);
		then:"Persist The Record"
			 loanApplicationService.saveLoanApplicationDetails(loanAppl) >>  application;
	}
	

}
